{ pkgs, lib, ... }:

with lib;

{
  imports = [
    <nixpkgs/nixos/modules/installer/cd-dvd/installation-cd-graphical-base.nix>
    <nixpkgs/nixos/modules/installer/cd-dvd/channel.nix>
  ];

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";
  console = {
    font = "Lat2-Terminus16";
    keyMap = "dvorak";
  };

  isoImage.edition = "xfce";

  services.xserver = {
    desktopManager.xfce.enable = true;
    displayManager = {
      lightdm = {
	enable = true;
      };
      autoLogin = {
	enable = true;
	user = "nixos";
      };
    };
    layout = "us";
    xkbVariant = "dvorak";
  };

  environment.systemPackages = with pkgs; [
    tmux
    screen  # Serial terminals

    nano
    ed
    vim
    mg
    emacs
    ispell  # Emacs needs this for flyspell.

    qutebrowser

    nethack
  ];
}
